﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Abstract Messages for Caller" Type="Folder"/>
	<Item Name="Config" Type="Folder">
		<Item Name="Example.cnfg" Type="Document" URL="../Example.cnfg"/>
	</Item>
	<Item Name="InstanceValues Classes" Type="Folder">
		<Item Name="InstanceValue.lvclass" Type="LVClass" URL="../InstanceValueClasses/InstanceValue/InstanceValue.lvclass"/>
		<Item Name="InstanceValue_2dData.lvclass" Type="LVClass" URL="../InstanceValueClasses/InstanceValue_2dData/InstanceValue_2dData.lvclass"/>
		<Item Name="InstanceValue_boolean.lvclass" Type="LVClass" URL="../InstanceValueClasses/InstanceValue_boolean/InstanceValue_boolean.lvclass"/>
		<Item Name="InstanceValue_image.lvclass" Type="LVClass" URL="../InstanceValueClasses/InstanceValue_image/InstanceValue_image.lvclass"/>
		<Item Name="InstanceValue_numeric.lvclass" Type="LVClass" URL="../InstanceValueClasses/InstanceValue_numeric/InstanceValue_numeric.lvclass"/>
		<Item Name="InstanceValue_string.lvclass" Type="LVClass" URL="../InstanceValueClasses/InstanceValue_string/InstanceValue_string.lvclass"/>
	</Item>
	<Item Name="Messages for this Actor" Type="Folder">
		<Item Name="After Launch Init Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/After Launch Init Msg/After Launch Init Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Check State Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Check State Msg/Check State Msg.lvclass"/>
		<Item Name="Connect to DB Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Connect to DB Msg/Connect to DB Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Create New Shot Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Create New Shot Msg/Create New Shot Msg.lvclass"/>
		<Item Name="Disconnect from DB Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Disconnect from DB Msg/Disconnect from DB Msg.lvclass"/>
		<Item Name="Write 2d Value Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Write 2d Value Msg/Write 2d Value Msg.lvclass"/>
		<Item Name="Write Boolean Value Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Write Boolean Value Msg/Write Boolean Value Msg.lvclass"/>
		<Item Name="Write Image Value Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Write Image Value Msg/Write Image Value Msg.lvclass"/>
		<Item Name="Write Numeric Value Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Write Numeric Value Msg/Write Numeric Value Msg.lvclass"/>
		<Item Name="Write Shot Description Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Write Shot Description Msg/Write Shot Description Msg.lvclass"/>
		<Item Name="Write String Value Msg.lvclass" Type="LVClass" URL="../POLARIS DB Proxy Messages/Write String Value Msg/Write String Value Msg.lvclass"/>
	</Item>
	<Item Name="Tests" Type="Folder">
		<Item Name="Non Actor VI Test.vi" Type="VI" URL="../Non Actor VI Test.vi"/>
	</Item>
	<Item Name="POLARIS DB Proxy.lvclass" Type="LVClass" URL="../POLARIS DB Proxy/POLARIS DB Proxy.lvclass"/>
</Library>
